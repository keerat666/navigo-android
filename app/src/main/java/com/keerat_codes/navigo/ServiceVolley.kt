package com.keerat_codes.navigo

import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonArrayRequest
import org.json.JSONArray
import org.json.JSONObject

class ServiceVolley : ServiceInterface {
    override fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray) -> Unit)
    {
        Log.d("Base Path",path)
        val jsonObjReq = object : JsonArrayRequest(Method.POST, path, params,
            Response.Listener<JSONArray> { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                Log.d("Response",response.toString())

                completionHandler(response)
            },
            Response.ErrorListener { error ->
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                Log.d("Error",error.message)
                val resp=JSONArray(error.message)
                completionHandler(resp)
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        BackendVolley.instance?.addToRequestQueue(jsonObjReq, TAG)
    }

    val TAG = ServiceVolley::class.java.simpleName
//    val basePath = "https://your/backend/api/"

    override fun get(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit) {

        Log.d("Base Path",path)
        val jsonObjReq = object : JsonArrayRequest(Method.GET, path, params,
            Response.Listener<JSONArray> { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                Log.d("Response",response.toString())

                completionHandler(response)
            },
            Response.ErrorListener { error ->
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                Log.d("Error",error.toString())
                //val resp=JSONArray(error.message)
                completionHandler(null)
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        BackendVolley.instance?.addToRequestQueue(jsonObjReq, TAG)
    }
}