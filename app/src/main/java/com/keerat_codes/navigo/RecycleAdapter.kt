package com.keerat_codes.navigo;
import android.content.Context;
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.card_category.view.*


class RecycleAdapter(val items:ArrayList<String>,val context:Context): RecyclerView.Adapter <ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.card_category, p0, false))
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.binditems(items.get(p1))
    }

    override fun getItemCount(): Int {
        return items.size;
    }
}
    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        fun binditems(users:String)
        {
           itemView.textView2.text=users;

            itemView.findViewById<LinearLayout>(R.id.card).setOnClickListener {

                val intent= Intent(itemView.context,HomeScreen::class.java)
                itemView.context.startActivity(intent)
            }

        }

}
