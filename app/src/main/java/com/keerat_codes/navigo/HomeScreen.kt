package com.keerat_codes.navigo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_home_screen.*
import java.util.ArrayList

class HomeScreen : AppCompatActivity() {

    val trending_movies: ArrayList<String> = ArrayList()
    val genres: ArrayList<String> = ArrayList()
    val music: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        addContent()
        addGenres()
        addmusic()
        trendingmovies.layoutManager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        trendingmovies.layoutManager= GridLayoutManager(this,1,GridLayoutManager.HORIZONTAL,false)
        trendingmovies.adapter=Trending_Movies(trending_movies,this)



        hero_banner.layoutManager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        hero_banner.layoutManager= GridLayoutManager(this,1,GridLayoutManager.HORIZONTAL,false)
        hero_banner.adapter=Hero_banner(trending_movies,this)

        genre_cards.layoutManager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        genre_cards.layoutManager= GridLayoutManager(this,1,GridLayoutManager.HORIZONTAL,false)
        genre_cards.adapter=Categories_card(genres,this)

        latest_audio.layoutManager= LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        latest_audio.layoutManager= GridLayoutManager(this,1,GridLayoutManager.HORIZONTAL,false)
        latest_audio.adapter=Latest_Music(music,this)
    }

    fun addContent()
    {

        trending_movies.add("Inception")
        trending_movies.add("Interstellar")
        trending_movies.add("Dark Knight")
        trending_movies.add("Dark Knight Rises")
        trending_movies.add("Batman Begins")
        trending_movies.add("Dunkirk")
        trending_movies.add("Insomnia")
        trending_movies.add("Memento")

    }

    fun addGenres()
    {
        genres.add("Action")
        genres.add("Adventure")
        genres.add("Thriller")
        genres.add("Comedy")
        genres.add("Romance")
        genres.add("See More")

    }

    fun addmusic()
    {
        music.add("Bollywood")
        music.add("Rock")
        music.add("Pop")
        music.add("Metal")
        music.add("Hip-Hop")
        music.add("See More")

    }
}
