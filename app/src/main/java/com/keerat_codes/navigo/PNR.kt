package com.keerat_codes.navigo

import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_pnr.*
import org.json.JSONArray

class PNR : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pnr)
        var PRIVATE_MODE = 0
        val PREF_NAME = "navigo"
        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)

        val dive_in = findViewById<Button>(R.id.verify)

        dive_in.setOnClickListener {
            pnrcheck()

        }
    }

    fun pnrcheck() {
        val service = ServiceVolley()
        val apiController = APIController(service)

        val path = "http://192.168.43.81:8884/user/getUser/" + pnrinput.text
        val params = JSONArray()
        try {
            apiController.get(path, params) { response ->
                // Parse the result

                if(response!=null)
                {
                    Toasty.success(this, "Welcome to Navigo!", Toast.LENGTH_LONG, true).show();
                    Log.d("Response-PNR", response.toString())
                     intent= Intent(applicationContext,prefrences::class.java)
                    intent.putExtra("pnr",pnrinput.text)
                    startActivity(intent)
                }
                else
                {
                    Toasty.warning(this, "PNR does not exist.Please try again.", Toast.LENGTH_LONG, true)
                            .show();
                }


            }
        } catch (e: Exception) {
            Toasty.warning(this, "PNR does not exist.Please try again.", Toast.LENGTH_LONG, true)
                .show();

        }
    }

}
