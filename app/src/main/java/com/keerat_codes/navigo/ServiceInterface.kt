package com.keerat_codes.navigo

import org.json.JSONArray
import org.json.JSONObject

interface ServiceInterface {
    fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray) -> Unit)
    fun get(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit)

}