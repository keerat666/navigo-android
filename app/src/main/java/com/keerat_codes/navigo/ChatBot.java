package com.keerat_codes.navigo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class ChatBot extends AppCompatActivity {
    RecyclerView recyclerView;
    EditText editText;
    RelativeLayout addBtn;
    //    DatabaseReference ref;
//    FirebaseRecyclerAdapter<ChatMessage,chat_rec> adapter;
    Boolean flagFab = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_bot2);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        editText = (EditText) findViewById(R.id.editText);
        addBtn = (RelativeLayout) findViewById(R.id.addBtn);

        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

//        ref = FirebaseDatabase.getInstance().getReference();
//        ref.keepSynced(true);


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = editText.getText().toString().trim();

//                if (!message.equals("")) {
//
//                    ChatMessage chatMessage = new ChatMessage(message, "user");
//                    ref.child("chat").push().setValue(chatMessage);
//
//                }

                editText.setText("");

            }
        });


//        adapter = new FirebaseRecyclerAdapter<ChatMessage, chat_rec>(ChatMessage.class, R.layout.msglist, chat_rec.class, ref.child("chat")) {
//            @Override
//            protected void populateViewHolder(chat_rec viewHolder, ChatMessage model, int position) {
//
//                if (model.getMsgUser().equals("user")) {
//
//
//                    viewHolder.rightText.setText(model.getMsgText());
//
//                    viewHolder.rightText.setVisibility(View.VISIBLE);
//                    viewHolder.leftText.setVisibility(View.GONE);
//                } else {
//                    viewHolder.leftText.setText(model.getMsgText());
//
//                    viewHolder.rightText.setVisibility(View.GONE);
//                    viewHolder.leftText.setVisibility(View.VISIBLE);
//                }
//            }
//        };
//
//
//        recyclerView.setAdapter(adapter);


    }
}

