package com.keerat_codes.navigo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_prefrences.*
import java.lang.reflect.GenericArrayType
import java.util.ArrayList

class prefrences : AppCompatActivity() {

    val genres: ArrayList<String> = ArrayList()
    var genres2: ArrayList<String> = ArrayList()
    var choosen:ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prefrences)


        addContent()
        addContent2()
        prefs.layoutManager= LinearLayoutManager(this)
        prefs.layoutManager= GridLayoutManager(this,3)
        prefs.adapter=prefrences_block(genres,this)


        val dive_in=findViewById<Button>(R.id.verify)

        dive_in.setOnClickListener {

            var x=prefrences_block(genres,applicationContext).getOptions()
            var i=0;
            if (x != null) {
                for(y in x)
                {

                    if(y=="1")
                    {
                        Log.d("Choosen Ones",genres2.get(i))
                        choosen.add(genres2.get(i))
                    }

                    i += 1
                }
            }


            Log.d("Prefrences",choosen.toString())

            intent= Intent(applicationContext,Homev2::class.java)
            startActivity(intent)
        }


    }

    fun addContent()
    {
        genres.add("Action")
        genres.add("Adventure")
        genres.add("Thriller")
        genres.add("Comedy")
        genres.add("Romance")
        genres.add("Horror")
        genres.add("Science Fiction")
        genres.add("Biographies")

    }

    fun addContent2()
    {
        genres2.add("Action")
        genres2.add("Adventure")
        genres2.add("Thriller")
        genres2.add("Comedy")
        genres2.add("Romance")
        genres2.add("Horror")
        genres2.add("Science Fiction")
        genres2.add("Biographies")

    }
}
