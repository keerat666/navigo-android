package com.keerat_codes.navigo;
import android.content.Context;
import android.graphics.Color
import android.support.v7.widget.RecyclerView;
import android.util.Log
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.card_category.view.card
import kotlinx.android.synthetic.main.card_category.view.textView2

var state: ArrayList<String>? = null;

class prefrences_block(val items: ArrayList<String>?, val context:Context): RecyclerView.Adapter <ViewHolder_p>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder_p{
        state=items
        return ViewHolder_p(LayoutInflater.from(context).inflate(R.layout.genre_cards, p0, false))
    }

    override fun onBindViewHolder(p0: ViewHolder_p, p1: Int) {
        p0.binditems(items?.get(p1).toString(),p1)
    }

    override fun getItemCount(): Int {
        return items!!.size;
    }

    fun getOptions(): ArrayList<String>? {
        return state;
    }


}
class ViewHolder_p(itemView:View):RecyclerView.ViewHolder(itemView)
{
    fun binditems(users:String,current:Int)
    {
        itemView.textView2.text=users;

        itemView.findViewById<LinearLayout>(R.id.card).setOnClickListener {
            if(state?.get(current)!= "1" && state?.get(current)!= "0" )
            {
                state?.set(current, "1");
                itemView.card.setBackgroundColor(Color.parseColor("#ffffff"))
                Log.d("Null",""+ state?.get(current))

            }
            else if(state?.get(current)== "1")
            {
                state?.set(current, "0");
                itemView.card.setBackgroundColor(Color.parseColor("#FFB900"))
                Log.d("State Current 1",""+ state?.get(current))

            }
            else if(state?.get(current)== "0")
            {
                state?.set(current, "1");
                itemView.card.setBackgroundColor(Color.parseColor("#ffffff"))
                Log.d("State Current 0",""+ state?.get(current))

            }



        }

    }

}


