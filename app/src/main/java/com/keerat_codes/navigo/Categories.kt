package com.keerat_codes.navigo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_categories.*
import java.lang.Exception
import java.util.ArrayList

class Categories : AppCompatActivity() {

    val genres:ArrayList<String> = ArrayList()
    val music: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        addContent()
        addmusic()
        try{
            var x=intent.getStringExtra("type")
            if(x=="audio")
            {
                categories.text="Audio Categories"
                recycle.layoutManager= LinearLayoutManager(this)
                recycle.layoutManager= GridLayoutManager(this,1)
                recycle.adapter=RecycleAdapter(music,this)
            }
            else
            {
                categories.text="Video Categories"
                recycle.layoutManager= LinearLayoutManager(this)
                recycle.layoutManager= GridLayoutManager(this,1)
                recycle.adapter=RecycleAdapter(genres,this)
            }


        }catch(e:Exception)
        {
            //TODO
        }


    }

    fun addContent()
    {
        genres.add("Action")
        genres.add("Adventure")
        genres.add("Thriller")
        genres.add("Comedy")
        genres.add("Romance")
        genres.add("Horror")
        genres.add("Science Fiction")
        genres.add("Biographies")
    }

    fun addmusic()
    {
        music.add("Bollywood")
        music.add("Rock")

        music.add("Pop")

        music.add("Metal")

        music.add("Hip-Hop")
        music.add("Retro")
        music.add("Rap")

    }

}
