package com.keerat_codes.navigo;
import android.content.Context;
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.card_category.view.*
import kotlinx.android.synthetic.main.card_category.view.textView2
import kotlinx.android.synthetic.main.categories_card.view.*
import kotlinx.android.synthetic.main.latest_music.view.*


class Latest_Music(val items:ArrayList<String>,val context:Context): RecyclerView.Adapter <ViewHolder5>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder5 {
        return ViewHolder5(LayoutInflater.from(context).inflate(R.layout.latest_music, p0, false))
    }

    override fun onBindViewHolder(p0: ViewHolder5, p1: Int) {
        p0.binditems(items.get(p1))
    }

    override fun getItemCount(): Int {
        return items.size;
    }
}
class ViewHolder5(itemView:View):RecyclerView.ViewHolder(itemView)
{
    fun binditems(users:String)
    {
        itemView.textView2.text=users;
        var colorset= arrayOf("#F4FF52","#F5B841","#E6E6E6","#8093F1","#D6CFCB","#AED4E6","#DDF8E8","#DDBDD5","#E7E08B")
        val random=(0..8).shuffled().first()
        itemView.music_genre_background.setBackgroundColor(Color.parseColor(colorset.get(random)))
        itemView.findViewById<LinearLayout>(R.id.card).setOnClickListener {

            if(users=="See More")
            {
                val intent= Intent(itemView.context,Categories::class.java)
                intent.putExtra("type","audio")
                itemView.context.startActivity(intent)
            }
            else
            {
                val intent= Intent(itemView.context,movie_gallery::class.java)
                intent.putExtra("category",users)
                itemView.context.startActivity(intent)
            }

        }

    }

}
