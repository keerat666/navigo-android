package com.keerat_codes.navigo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dive_in=findViewById<Button>(R.id.angry_btn)

        dive_in.setOnClickListener {

            intent= Intent(applicationContext,PNR::class.java)
            startActivity(intent)
        }
    }
}
