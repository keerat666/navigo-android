package com.keerat_codes.navigo;
import android.content.Context;
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.card_category.view.*
import kotlinx.android.synthetic.main.card_category.view.textView2
import kotlinx.android.synthetic.main.categories_card.view.*


class Categories_card(val items:ArrayList<String>,val context:Context): RecyclerView.Adapter <ViewHolder4>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder4 {
        return ViewHolder4(LayoutInflater.from(context).inflate(R.layout.categories_card, p0, false))
    }

    override fun onBindViewHolder(p0: ViewHolder4, p1: Int) {
        p0.binditems(items.get(p1))
    }

    override fun getItemCount(): Int {
        return items.size;
    }
}
class ViewHolder4(itemView:View):RecyclerView.ViewHolder(itemView)
{
    fun binditems(users:String)
    {
        itemView.textView2.text=users;
        var colorset= arrayOf("#4A313D","#1A090D","#6B6570","#101935","#34113F","#0c8346","#554971","#B47AEA","#6A041D")
        val random=(0..8).shuffled().first()
        itemView.genrebox.setBackgroundColor(Color.parseColor(colorset.get(random)))
        itemView.findViewById<LinearLayout>(R.id.card).setOnClickListener {

            if(users=="See More")
            {
                val intent= Intent(itemView.context,Categories::class.java)
                itemView.context.startActivity(intent)
            }
            else
            {
                val intent= Intent(itemView.context,movie_gallery::class.java)
                intent.putExtra("category",users)
                itemView.context.startActivity(intent)
            }



        }

    }

}
