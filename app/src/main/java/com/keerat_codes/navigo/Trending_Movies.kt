package com.keerat_codes.navigo

import android.app.Application
import android.content.Context;
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView;
import android.util.Log
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_category.view.*
import kotlinx.android.synthetic.main.trending.view.*
import java.net.URL


class Trending_Movies(val items:ArrayList<String>,val context:Context): RecyclerView.Adapter <ViewHolder1>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder1 {
        return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.trending, p0, false))
    }

    override fun onBindViewHolder(p0: ViewHolder1, p1: Int) {
        p0.binditems(items.get(p1),context)
    }

    override fun getItemCount(): Int {
        return items.size;
    }
}
class ViewHolder1(itemView:View):RecyclerView.ViewHolder(itemView)
{
    fun binditems(users:String, context:Context)
    {
        var x =users
        Log.d("Media link",x)

        x="http://"+x
//
        Picasso.get().load(x).placeholder(R.drawable.inception).into(itemView.poster)
//
        //Glide.with(context).load(x).into(itemView.poster)

        itemView.findViewById<LinearLayout>(R.id.card).setOnClickListener {
            val intent= Intent(itemView.context,Media_Page::class.java)
//            val imglink=URL("http://"+x)
//            val bmp=BitmapFactory.decodeStream(imglink.openConnection().getInputStream())
//            itemView.poster.setImageBitmap(bmp)

            itemView.context.startActivity(intent)

        }

    }

}
