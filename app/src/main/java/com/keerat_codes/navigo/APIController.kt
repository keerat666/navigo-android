package com.keerat_codes.navigo

import org.json.JSONArray
import org.json.JSONObject

class APIController constructor(serviceInjection: ServiceInterface): ServiceInterface {
    override fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray) -> Unit)
    {

        service.post(path, params, completionHandler)

    }

    private val service: ServiceInterface = serviceInjection

    override fun get(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit) {
        service.get(path, params, completionHandler)
    }
}