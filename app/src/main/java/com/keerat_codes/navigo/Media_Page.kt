package com.keerat_codes.navigo

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.MediaController
import android.widget.Toast
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_video_stream.*

class Media_Page : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media__page)

        val play=findViewById<Button>(R.id.play)

        play.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                Log.d("Stream status","Available on this device.")
                intent= Intent(applicationContext,VideoStream::class.java)
                startActivity(intent)
            }
            else
            {
                Toasty.info(this, "Streaming content in boosted mode for low end devices", Toast.LENGTH_SHORT, true).show();

                intent= Intent(applicationContext,VideoStreamLow::class.java)
                startActivity(intent)
            }


        }
    }
}
