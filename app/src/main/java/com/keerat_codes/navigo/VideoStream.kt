package com.keerat_codes.navigo

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.MediaController
import android.widget.VideoView
import java.lang.Exception

class VideoStream : AppCompatActivity() {

    private var vv: VideoView? = null
    private var mediacontroller: MediaController? = null
    private var uri: Uri? = null
    private var bwindow:WebView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_stream)
        requestedOrientation=ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        vv = findViewById(R.id.vv) as VideoView
        //get url from intent
//        bwindow=findViewById(R.id.window) as WebView
//        bwindow!!.settings.allowContentAccess
//        bwindow!!.settings.allowFileAccessFromFileURLs
//        bwindow!!.webViewClient=WebViewClient()
//        bwindow!!.settings.builtInZoomControls
//        bwindow!!.settings.javaScriptEnabled
//
//        bwindow!!.loadUrl("http://192.168.43.193/interstellar.mp4")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mediacontroller = MediaController(this)
            mediacontroller!!.setAnchorView(vv)
            Log.d("Stream status","Available on this device.")

        }
        else
        {
            Log.d("Stream status","Not available on this device.")
        }

        try{
            vv!!.setMediaController(mediacontroller)
            vv!!.setVideoPath("http://192.168.43.81:80/media/video/3 peg.mp4")
            vv!!.requestFocus()
            vv!!.start()
        }catch(e:Exception)
        {
            Log.d("Video Exception:",e.toString())
        }
//
    }
}
