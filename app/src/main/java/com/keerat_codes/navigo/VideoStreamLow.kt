package com.keerat_codes.navigo

import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient

class VideoStreamLow : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_stream_low)
        requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        //get url from intent

        var bwindow: WebView?=null
        bwindow=findViewById(R.id.window) as WebView
        bwindow!!.settings.allowContentAccess
        bwindow!!.settings.allowFileAccessFromFileURLs
        bwindow!!.webViewClient= WebViewClient()
        bwindow!!.settings.builtInZoomControls
        bwindow!!.settings.javaScriptEnabled

        bwindow!!.loadUrl("http://192.168.43.81:80/media/video/3 peg.mp4")
    }

}
