package com.keerat_codes.navigo

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Home.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */
class Home : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    val trending_movies: ArrayList<String> = ArrayList()
    val top4_movies: ArrayList<String> = ArrayList()

    val genres: ArrayList<String> = ArrayList()
    val music: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        addGenres()
        addmusic()
        load()
        top4()
        val view: View = inflater.inflate(R.layout.fragment_home, container,
            false)




        view.hero_banner.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        view.hero_banner.layoutManager= GridLayoutManager(context,1, GridLayoutManager.HORIZONTAL,false)
        view.hero_banner.adapter= context?.let { Hero_banner(top4_movies, it) }

        view.genre_cards.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        view.genre_cards.layoutManager= GridLayoutManager(context,1, GridLayoutManager.HORIZONTAL,false)
        view.genre_cards.adapter= context?.let { Categories_card(genres, it) }

        view.latest_audio.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        view.latest_audio.layoutManager= GridLayoutManager(context,1, GridLayoutManager.HORIZONTAL,false)
        view.latest_audio.adapter= context?.let { Latest_Music(music, it) }
        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
//    fun onButtonPressed(uri: Uri) {
//        listener?.onFragmentInteraction(uri)
//    }

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
//    }

//    override fun onDetach() {
//        super.onDetach()
//        listener = null
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Home.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Home().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


    fun addContent(response: JSONArray)
    {

       Log.d("Top 4",response.toString())

        for(i in 0 until response.length())
        {
            val item=response.getJSONObject(i)
            Log.d("Movie Name",item.toString())
            trending_movies.add(item.getString("thumbnailPath"))
        }
        view!!.trendingmovies.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        view!!.trendingmovies.layoutManager= GridLayoutManager(context,1, GridLayoutManager.HORIZONTAL,false)
        view!!.trendingmovies.adapter= context?.let { Trending_Movies(trending_movies, it) }



    }

    fun addGenres()
    {
        genres.add("Action")
        genres.add("Adventure")
        genres.add("Thriller")
        genres.add("Comedy")
        genres.add("Romance")
        genres.add("See More")

    }

    fun addmusic()
    {
        music.add("Bollywood")
        music.add("Rock")
        music.add("Pop")
        music.add("Metal")
        music.add("Hip-Hop")
        music.add("See More")

    }

    fun top4()
    {
        top4_movies.add("Bollywood")
        top4_movies.add("Bollywood")
        top4_movies.add("Bollywood")
        top4_movies.add("Bollywood")

    }

    fun load()
    {
        val service = ServiceVolley()
        val apiController = APIController(service)

        val path = "http://192.168.43.81:8884/media/getTopMovies"
        val params = JSONArray()


        apiController.get(path, params) { response ->
            // Parse the result

            Log.d("Response-Home",response.toString())
                addContent(response!!)

        }
    }

}
