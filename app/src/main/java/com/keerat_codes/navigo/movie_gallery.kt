package com.keerat_codes.navigo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import kotlinx.android.synthetic.main.activity_movie_gallery.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

class movie_gallery : AppCompatActivity() {

    val movies: ArrayList<String> = ArrayList()
    var cat="";
    val result_movies: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_gallery)
        cat=intent.getStringExtra("category")
        load()


    }

    fun addContent(response: JSONArray)
    {

        for(i in 0 until response.length())
        {
            val item=response.getJSONObject(i)
            Log.d("Movie Name",item.toString())
            result_movies.add(item.getString("thumbnailPath"))
        }

//        gallery.layoutManager= LinearLayoutManager(this) as RecyclerView.LayoutManager?
//        gallery.layoutManager= GridLayoutManager(this,2)
//        gallery.adapter=gallery_recycle(result_movies,this)

    }


    fun load()
    {
        val service = ServiceVolley()
        val apiController = APIController(service)

        val path = "http://192.168.43.81:8884/media/getMoviesByCategory/"+cat
        val params = JSONArray()
//        params.put("category",cat)

        apiController.get(path, params) { response ->
            // Parse the result

            Log.d("Response-Gallery",response.toString())
            addContent(response!!)

        }
    }
}
