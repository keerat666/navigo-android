package com.keerat_codes.navigo

import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.MenuItem

import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import es.dmoral.toasty.Toasty
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.CountDownTimer
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.Toast


class Homev2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homev2)
//        val type = Typeface.createFromAsset(assets, "font/montaga.ttf")
        Toasty.Config.getInstance()
//            .setToastTypeface(type) // optional
            .setTextSize(10) // optional
            .apply(); // required


        Toasty.info(this@Homev2, "Welcome to Navigo", Toast.LENGTH_SHORT, true).show();

        val fragment = Home()
        loadFragment(fragment)
        val navigation = findViewById(R.id.navigation) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    val mOnNavigationItemSelectedListener = object:
        BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(@NonNull item: MenuItem):Boolean {
            val fragment: Fragment
            when (item.getItemId()) {
                R.id.navigation_shop -> {
                    fragment = Home()
                    loadFragment(fragment)
                    return true
                }
                R.id.navigation_gifts -> {

                    fragment = Map()
                    loadFragment(fragment)

                    return true
                }

                R.id.navigation_profile -> {
                    fragment = profile()
                    loadFragment(fragment)
                    return true
                }
            }
            return false
        }
    }


    fun loadFragment(fragment:Fragment)
    {
        val transaction = getSupportFragmentManager().beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
